package com.confusedpenguins.socialdeets;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        assertEquals("com.confusedpenguins.socialdeets", appContext.getPackageName());
    }

    /**
     * Ensure that no one has fiddled with the bare essential permissions
     * More could be added but without these app will fatal.
     * New mandatory permissions should be added to this test when needed.
     *
     * @throws Exception
     */
    @Test
    public void confirmBasicPermissions() throws Exception {
        boolean missingPermission = false;
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        String[] facebookPermissions = appContext.getResources().getStringArray(R.array.facebook_permissions);
        String[] expectedMandatoryPermissions = {"public_profile", "email", "user_hometown", "user_likes"};

        for (String expectedPermission : expectedMandatoryPermissions) {
            boolean foundPermission = false;
            for (String applicationPermission : facebookPermissions) {
                if (applicationPermission.equals(expectedPermission)) {
                    foundPermission = true;
                }
            }
            if (!foundPermission) {
                missingPermission = true;
                break;
            }
        }
        assertEquals("Basic Permissions Missing", false, missingPermission);
    }

    /**
     * Ensure that no one has fiddled with the bare essential fields
     * More could be added but without these app will fatal.
     * New mandatory fields should be added to this test when needed.
     *
     * @throws Exception
     */
    @Test
    public void confirmBaseFacebookFields() throws Exception {
        boolean missingPermission = false;
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();
        String[] facebook_profile_fields = appContext.getResources().getStringArray(R.array.facebook_profile_fields);
        String[] expectedMandatoryFields = {"id", "name", "email", "picture.type(large)", "hometown"};

        for (String expectedField : expectedMandatoryFields) {
            boolean foundPermission = false;
            for (String actualField : facebook_profile_fields) {
                if (actualField.equals(expectedField)) {
                    foundPermission = true;
                }
            }
            if (!foundPermission) {
                missingPermission = true;
                break;
            }
        }
        assertEquals("Basic Field Missing", false, missingPermission);
    }
}