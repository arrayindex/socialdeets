package com.confusedpenguins.socialdeets;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.not;


/**
 * Created by Andrew on 6/26/2017.
 */
@RunWith(AndroidJUnit4.class)
public class FacebookLoginFragmentTest {
    @Rule
    public ActivityTestRule<MainActivity> activityTestRule = new ActivityTestRule<>(MainActivity.class);

    /**
     * Check if logged in, then log out and make sure that the UI updates accordingly then log in again
     * Else login and check to see if the Login does the work then logout.
     * @throws Exception
     */
    @Test
    public void testFacebookLogin() throws Exception {
        if (activityTestRule.getActivity().isLoggedIn()) {
            onView(withId(R.id.user_email)).check(matches(isDisplayed()));
            onView(withId(R.id.user_name)).check(matches(isDisplayed()));
            onView(withId(R.id.user_hometown)).check(matches(isDisplayed()));
        } else {
            onView(withId(R.id.login_button)).perform(click());
            onView(withId(R.id.user_email)).check(matches(isDisplayed()));
            onView(withId(R.id.user_name)).check(matches(isDisplayed()));
            onView(withId(R.id.user_hometown)).check(matches(isDisplayed()));
        }
    }
}
