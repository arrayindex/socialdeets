package com.confusedpenguins.socialdeets;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.confusedpenguins.socialdeets.fragments.DataMinerFragment;
import com.confusedpenguins.socialdeets.fragments.FacebookLoginFragment;
import com.confusedpenguins.socialdeets.fragments.ProfileViewFragment;
import com.crashlytics.android.Crashlytics;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;

import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {
    private Fragment facebookLoginFragment;
    private Fragment profileViewFragment;
    private Fragment dataMinerFragment;

    private AccessTokenTracker accessTokenTracker;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (isLoggedIn()) {
            MenuInflater menuInflater = getMenuInflater();
            menuInflater.inflate(R.menu.menu_main, menu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_data_miner:
                openDataMiner();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        savedInstanceState = null;
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main);

        if (isLoggedIn()) {
            openProfileView();
        } else {
            openLoginView();
        }
        accessTokenTracker = new SocialAccessTokenTracker();
    }

    protected boolean isLoggedIn() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        if (accessToken != null && !accessToken.isExpired()) {
            return true;
        }
        return false;
    }


    private void openDataMiner() {
        dataMinerFragment = new DataMinerFragment();
        Log.d(MainActivity.class.getName(), "Adding DataMiner View");

        Bundle args = new Bundle();
        args.putString(getString(R.string.key_user_id), AccessToken.getCurrentAccessToken().getUserId());
        dataMinerFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.activity_main, dataMinerFragment)
                .addToBackStack("dataMinerFragment")
                .commit();
    }

    private void openProfileView() {
        profileViewFragment = new ProfileViewFragment();
        Log.d(MainActivity.class.getName(), "Adding Profile View");
        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .replace(R.id.activity_main, profileViewFragment)
                .commit();
        openLoginView();
    }

    private void openLoginView() {
        facebookLoginFragment = new FacebookLoginFragment();
        Bundle args = new Bundle();
        args.putBoolean(getString(R.string.key_logged_in), isLoggedIn());
        facebookLoginFragment.setArguments(args);

        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                .add(R.id.activity_main, facebookLoginFragment)
                .commit();


    }

    public void notifyUser(String message) {
        if (message != null) {
            Toast toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private class SocialAccessTokenTracker extends AccessTokenTracker {
        @Override
        protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
            if (currentAccessToken == null && MainActivity.this.profileViewFragment != null) {
                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .remove(profileViewFragment)
                        .commitAllowingStateLoss();

                getSupportFragmentManager().beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .remove(facebookLoginFragment)
                        .commitAllowingStateLoss();

                openLoginView();
            }
            if (currentAccessToken != null && !currentAccessToken.isExpired()) {
                openProfileView();
            }

            invalidateOptionsMenu();
        }
    }

    /**
     * Ensure that fragments Fragment Manager get cleaned up
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.exit(0);
    }
}
