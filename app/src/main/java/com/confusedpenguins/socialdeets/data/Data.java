package com.confusedpenguins.socialdeets.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Andrew on 6/24/2017.
 */

public class Data implements Serializable {
    @SerializedName("url")
    private String url;

    @SerializedName("genre")
    private String genre;

    @SerializedName("awards")
    private String awards;


    public String getUrl() {
        return url;
    }

    public String getGenre() {
        return genre;
    }

    public String getAwards() {
        return awards;
    }

    @Override
    public String toString() {
        return "Data{" +
                "url='" + url + '\'' +
                '}';
    }
}
