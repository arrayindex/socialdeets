package com.confusedpenguins.socialdeets.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Andrew on 6/24/2017.
 */
public class Hometown implements Serializable {
    @SerializedName("id")
    private String id;

    @SerializedName("name")
    private String name;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Hometown{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
