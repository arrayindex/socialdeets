package com.confusedpenguins.socialdeets.data;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Andrew on 6/24/2017.
 */

public class Picture implements Serializable {
    @SerializedName("data")
    private Data data;

    public Data getData() {
        return data;
    }

    @Override
    public String toString() {
        return "Picture{" +
                "data=" + data +
                '}';
    }
}
