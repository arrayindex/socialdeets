package com.confusedpenguins.socialdeets.data;

import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Andrew on 6/24/2017.
 */
public class SocialProfile implements Serializable {

    @SerializedName("id")
    private String userId;

    @SerializedName("name")
    private String fullName;

    @SerializedName("email")
    private String email;

    @SerializedName("picture")
    private Picture picture;

    @SerializedName("hometown")
    private Hometown hometown;


    public String getUserId() {
        return userId;
    }

    public Picture getPicture() {
        return picture;
    }

    public Hometown getHometown() {
        return hometown;
    }

    public String getFullName() {
        return fullName;
    }

    public String getEmail() {
        return email;
    }

    public static SocialProfile serializeString(String jsonString) {
        try {
            Gson gson = new GsonBuilder().setPrettyPrinting().create();
            return gson.fromJson(jsonString, SocialProfile.class);
        } catch (Exception e) {
            Log.e(SocialProfile.class.getName(), "Serialize String Failed, please check input string");
            return null;
        }
    }
}