package com.confusedpenguins.socialdeets.data.movie;

import android.util.Log;

import java.util.HashMap;

/**
 * Created by Andrew on 6/24/2017.
 * <p>
 * I feel as if this should be done by a backend with alot more power than a mobile device but this implementation should prove suitable for now
 * <p>
 * Figure out the top genres, how many of their movies are award winning
 */
public class MovieAnalyzer {
    public enum Facet {
        GENRE, AWARD
    }

    private HashMap<String, Integer> movieLikeMap;
    private double totalMovies = 0;
    private double awardWinning = 0;

    public MovieAnalyzer() {
        movieLikeMap = new HashMap<>();
    }

    public synchronized void analyzeDataPoint(Facet facet, String rawString) {
        totalMovies++;

        if (rawString != null && facet != null) {
            // Passing in AWARD and whether the movie had an award is a big thing
            if (Facet.AWARD.equals(facet)) {
                awardWinning++;
            }
            String[] values = rawString.split(",|/");
            if (values.length > 0) {
                for (String dataPoint : values) {
                    if (Facet.GENRE.equals(facet)) {
                        dataPoint = dataPoint.trim().toLowerCase();
                        int count = movieLikeMap.containsKey(dataPoint) ? movieLikeMap.get(dataPoint) : 0;
                        movieLikeMap.put(dataPoint, count + 1);
                    }
                }
            }
        }
    }

    public double getAwardWinningPercentage() {
        Log.i(MovieAnalyzer.class.getName(), this.awardWinning + "");
        Log.i(MovieAnalyzer.class.getName(), this.totalMovies + "");
        return (this.awardWinning / totalMovies) * 100;
    }

    public String getFavoriteGenre() {
        int max = 0;
        String maxGenre = "";
        for (String key : movieLikeMap.keySet()) {
            if (movieLikeMap.get(key) > max) {
                max = movieLikeMap.get(key);
                maxGenre = key;
            }
        }
        return maxGenre;
    }
}