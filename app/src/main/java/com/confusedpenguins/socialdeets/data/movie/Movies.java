package com.confusedpenguins.socialdeets.data.movie;

import android.util.Log;

import com.confusedpenguins.socialdeets.data.Data;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Andrew on 6/24/2017.
 */

public class Movies implements Serializable {
    @SerializedName("data")
    private ArrayList<Data> data;

    public ArrayList<Data> getData() {
        return (data == null) ? new ArrayList<Data>() : data;
    }

    public static Movies serializeString(String jsonString) {
        try {
            if (jsonString == null) {
                return new Movies();
            } else {
                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                return gson.fromJson(jsonString, Movies.class);
            }
        } catch (Exception e) {
            Log.e(Movies.class.getName(), "Serialize String Failed, please check input string");
            return new Movies();
        }
    }
}
