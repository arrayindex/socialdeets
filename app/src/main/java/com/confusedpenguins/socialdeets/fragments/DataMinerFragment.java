package com.confusedpenguins.socialdeets.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.confusedpenguins.socialdeets.R;
import com.confusedpenguins.socialdeets.data.Data;
import com.confusedpenguins.socialdeets.data.movie.MovieAnalyzer;
import com.confusedpenguins.socialdeets.data.movie.Movies;
import com.confusedpenguins.socialdeets.utils.StringUtils;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Andrew on 6/26/2017.
 */

public class DataMinerFragment extends Fragment {
    View dataMinerFragment;

    MovieAnalyzer movieAnalyzer = null;

    /**
     * Executes the Movie Graph Request
     */
    private void executeMovieGraphRequest() {
        GetMovieData runner = new GetMovieData();
        runner.execute();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        dataMinerFragment = inflater.inflate(R.layout.fragment_data_miner, parent, false);

        if (dataMinerFragment != null && getArguments() != null && getArguments().getString(getString(R.string.key_user_id), null) != null) {
            this.movieAnalyzer = new MovieAnalyzer();
            executeMovieGraphRequest();
        }
        return dataMinerFragment;
    }

    private String snideComment(double percentage) {
        if (percentage > 50) {
            return getString(R.string.app_view_taste_good);
        } else {
            return getString(R.string.app_view_taste_bad);
        }
    }

    private boolean updateInterests() {
        String movieType = movieAnalyzer.getFavoriteGenre();
        if (movieType != null && movieType.length() > 0 && dataMinerFragment != null) {
            TextView userInterests = (TextView) dataMinerFragment.findViewById(R.id.user_interests);
            userInterests.setText(getString(R.string.app_view_genres_like).replace("{genre}", StringUtils.upperCaseFirstLetter(movieType)));
        }

        double awardWinningPercentage = movieAnalyzer.getAwardWinningPercentage();
        TextView userInterests = (TextView) dataMinerFragment.findViewById(R.id.user_taste);


        userInterests.setText(getString(R.string.app_view_taste_like)
                .replace("{percentage}", String.format("%.2f", awardWinningPercentage))
                .replace("{comment}", snideComment(awardWinningPercentage))
        );
        return true;
    }

    private class GetMovieData extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                final String[] afterString = {""};  // will contain the next page cursor
                final Boolean[] noData = {false};   // stop when there is no after cursor
                do {
                    Bundle bundleParams = new Bundle();
                    bundleParams.putString("after", afterString[0]);
                    new GraphRequest(AccessToken.getCurrentAccessToken(), "/" + getArguments().getString("userId", null) + "/movies?fields=genre,name,awards,directors", bundleParams, HttpMethod.GET, new GraphRequest.Callback() {
                        @Override
                        public void onCompleted(GraphResponse graphResponse) {
                            JSONObject jsonObject = graphResponse.getJSONObject();
                            try {
                                Log.d(ProfileViewFragment.class.getName(), graphResponse.getRawResponse());
                                for (Data movie : Movies.serializeString(graphResponse.getRawResponse()).getData()) {
                                    movieAnalyzer.analyzeDataPoint(MovieAnalyzer.Facet.GENRE, movie.getGenre());
                                    movieAnalyzer.analyzeDataPoint(MovieAnalyzer.Facet.AWARD, movie.getAwards());
                                }
                                if (!jsonObject.isNull("paging")) {
                                    JSONObject paging = jsonObject.getJSONObject("paging");
                                    JSONObject cursors = paging.getJSONObject("cursors");
                                    if (!cursors.isNull("after"))
                                        afterString[0] = cursors.getString("after");
                                    else
                                        noData[0] = true;
                                } else
                                    noData[0] = true;
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                    ).executeAndWait();
                } while (!noData[0] == true);
                return movieAnalyzer.getFavoriteGenre();
            } catch (Exception e) {
                e.printStackTrace();
                return "";
            }
        }

        @Override
        protected void onPostExecute(String result) {
            Log.d(ProfileViewFragment.class.getName(), result);
            updateInterests();
        }
    }
}
