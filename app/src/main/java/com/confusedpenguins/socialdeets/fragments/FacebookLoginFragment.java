package com.confusedpenguins.socialdeets.fragments;

import android.support.annotation.Nullable;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.confusedpenguins.socialdeets.MainActivity;
import com.confusedpenguins.socialdeets.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import java.util.Arrays;

/**
 * Created by Andrew on 6/23/2017.
 */
public class FacebookLoginFragment extends Fragment {
    private CallbackManager callbackManager;
    View facebookLoginFragment = null;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        facebookLoginFragment = inflater.inflate(R.layout.fragment_login, parent, false);
        LoginButton loginButton = (LoginButton) facebookLoginFragment.findViewById(R.id.login_button);
        loginButton.setReadPermissions(Arrays.asList(getActivity().getResources().getStringArray(R.array.facebook_permissions)));
        loginButton.setFragment(this);
        callbackManager = CallbackManager.Factory.create();
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                ((MainActivity) getActivity()).notifyUser(getString(R.string.app_login_success));
            }

            @Override
            public void onCancel() {
                Log.i(FacebookLoginFragment.class.getName(), "onCancel");
            }

            @Override
            public void onError(FacebookException exception) {
                ((MainActivity) getActivity()).notifyUser(getString(R.string.app_error_loading_login));
            }
        });

        chooseWelcomeScreen();
        return facebookLoginFragment;
    }

    private boolean chooseWelcomeScreen() {
        boolean isLoggedIn = getArguments().getBoolean(getString(R.string.key_logged_in));
        if(isLoggedIn) {
            facebookLoginFragment.findViewById(R.id.login_header).setVisibility(View.GONE);
        }
        return isLoggedIn;
    }
}
