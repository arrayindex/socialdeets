package com.confusedpenguins.socialdeets.fragments;

import android.support.annotation.Nullable;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageRequest;
import com.confusedpenguins.socialdeets.MainActivity;
import com.confusedpenguins.socialdeets.R;
import com.confusedpenguins.socialdeets.data.SocialProfile;
import com.confusedpenguins.socialdeets.utils.StringUtils;
import com.confusedpenguins.socialdeets.utils.network.RequestQueueManager;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import org.json.JSONObject;

/**
 * Created by Andrew on 6/24/2017.
 */
public class ProfileViewFragment extends Fragment {
    View profileViewFragment = null;
    private SocialProfile profile;


    private void initData() {
        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                profile = SocialProfile.serializeString(response.getRawResponse());
                if (profile != null) {
                    updateUI();
                } else {
                    ((MainActivity) getActivity()).notifyUser(getString(R.string.app_error_loading_profile));
                }
            }
        });
        String[] facebookProfileFields = getActivity().getResources().getStringArray(R.array.facebook_profile_fields);
        Bundle parameters = new Bundle();
        parameters.putString("fields", StringUtils.arrayToBasicString(facebookProfileFields));
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void updateUI() {
        if (profile == null && profileViewFragment != null) {
            ((MainActivity) getActivity()).notifyUser(getString(R.string.app_error_loading_profile));
        } else {
            TextView userName = (TextView) profileViewFragment.findViewById(R.id.user_name);
            userName.setText(profile.getFullName());

            TextView email = (TextView) profileViewFragment.findViewById(R.id.user_email);
            email.setText(profile.getEmail());

            if (profile.getHometown() != null) {
                TextView hometown = (TextView) profileViewFragment.findViewById(R.id.user_hometown);
                hometown.setText(profile.getHometown().getName());
            }
            final ImageView profilePic = (ImageView) profileViewFragment.findViewById(R.id.profile_image);
            final ImageRequest imageRequest = new ImageRequest(profile.getPicture().getData().getUrl(), new Response.Listener<Bitmap>() {
                @Override
                public void onResponse(Bitmap response) {
                    profilePic.setImageBitmap(response);
                }
            }, 0, 0, ImageView.ScaleType.CENTER_CROP, null, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    ((MainActivity) getActivity()).notifyUser(getString(R.string.app_error_loading_profile_picture));
                }
            });
            RequestQueueManager.getInstance(getActivity()).addToRequestQueue(imageRequest);
        }

    }



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        profileViewFragment = inflater.inflate(R.layout.fragment_profile, parent, false);
        if (profileViewFragment != null) {
            //Lets begin
            this.initData();
        }
        return profileViewFragment;
    }
}