package com.confusedpenguins.socialdeets.utils;

import com.confusedpenguins.socialdeets.R;

/**
 * Created by Andrew on 6/24/2017.
 */

public class StringUtils {
    /**
     * Intended to clean up field list for facebook api
     *
     * @param array
     * @return
     */
    public static String arrayToBasicString(String[] array) {
        if (array == null) {
            return "";
        }

        StringBuilder sb = new StringBuilder();
        for (int fieldIndex = 0; fieldIndex < array.length; fieldIndex++) {
            sb.append(array[fieldIndex]);
            if (fieldIndex != array.length - 1) {
                sb.append(",");
            }
        }
        return sb.toString();
    }

    public static String upperCaseFirstLetter(String input) {
        if (input != null && input.length() > 0) {
            return input.substring(0, 1).toUpperCase() + input.substring(1);
        }
        return "";
    }
}
