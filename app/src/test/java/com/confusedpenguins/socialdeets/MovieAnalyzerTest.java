package com.confusedpenguins.socialdeets;

import android.util.Log;

import com.confusedpenguins.socialdeets.data.movie.Movies;
import com.confusedpenguins.socialdeets.data.movie.MovieAnalyzer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by Andrew on 6/25/2017.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({Log.class})
public class MovieAnalyzerTest {
    @Before
    public void executedBeforeEach() {
        PowerMockito.mockStatic(Log.class);
    }

    @Test
    public void getMaxTest() throws Exception {
        MovieAnalyzer movieAnalyzer = new MovieAnalyzer();
        movieAnalyzer.analyzeDataPoint(MovieAnalyzer.Facet.GENRE, null);
        String actual = movieAnalyzer.getFavoriteGenre();
        assertEquals("The Analyzer should just return an empty string if nothing useful was injected", "", actual);
    }

    @Test
    public void addGenreStringTest() throws Exception {
        String[] genres = {"Horror", "HORROR", "HORROR ", "Action", "horror / action", "drama,romance,comedy"};
        MovieAnalyzer movieAnalyzer = new MovieAnalyzer();
        for (String genre : genres) {
            movieAnalyzer.analyzeDataPoint(MovieAnalyzer.Facet.GENRE, genre);
        }
        String actual = movieAnalyzer.getFavoriteGenre();
        assertEquals("Horror Movies are the best according to my list", "horror", actual);
    }

    @Test
    public void addAwardStringTest() throws Exception {
        String[] awards = {"Golden Globe", "Oscar", "Muffin", null, "RUFF", null};

        MovieAnalyzer movieAnalyzer = new MovieAnalyzer();
        for (String award : awards) {
            movieAnalyzer.analyzeDataPoint(MovieAnalyzer.Facet.AWARD, award);
        }
        double actual = movieAnalyzer.getAwardWinningPercentage();
        assertEquals("Horror Movies are the best according to my list", 66.666666, actual, 0.001);
    }

    @Test
    public void serializeStringTest() throws Exception {
        String jsonData = "{\\\"data\\\":[{\\\"id\\\":\\\"357451458332\\\"},{\\\"genre\\\":\\\"Drama\\\",\\\"id\\\":\\\"39644305296\\\"}],\\\"paging\\\":{\\\"cursors\\\":{\\\"before\\\":\\\"MzU3NDUxNDU4MzMy\\\",\\\"after\\\":\\\"Mzk2NDQzMDUyOTYZD\\\"},\\\"previous\\\":\\\"https:\\\\\\/\\\\\\/graph.facebook.com\\\\\\/v2.9\\\\\\/10102687105196604\\\\\\/movies?access_token=EAAQdzGvlYm8BAPL8RowIoiFG8633iRwC1ZCEvorXe6aZCda8D8WAlJkm33lXKjeF1qZAhr5U3Xgvuqc7TOuZC2jfUUP1CNL8nqEt4xx6pAaW0UT8MZCgQjWA7ZCzAz9RFxfvGLcEWwZAhtoJYWGNxE4GpZBGLhxSbSXVMpNDla0HlI4NDWNEBwOwY7FzfdxBmZBZB95i00jty2olbPO0WPjAjKZCy7XO3TRGVoZD&fields=genre&limit=25&before=MzU3NDUxNDU4MzMy\\\"}}";
        Movies movies = Movies.serializeString(jsonData);
        assertNotEquals("Serializer returned null", null, movies);
    }

    @Test
    public void serializeStringTestInvalidJSON() throws Exception {
        //Invalid JSON structure
        String jsonData = "{\\\"data\\\":{\\\"id\\\":\\\"357451458332\\\"},{\\\"genre\\\":\\\"Drama\\\",\\\"id\\\":\\\"39644305296\\\"}],\\\"paging\\\":{\\\"cursors\\\":{\\\"before\\\":\\\"MzU3NDUxNDU4MzMy\\\",\\\"after\\\":\\\"Mzk2NDQzMDUyOTYZD\\\"},\\\"previous\\\":\\\"https:\\\\\\/\\\\\\/graph.facebook.com\\\\\\/v2.9\\\\\\/10102687105196604\\\\\\/movies?access_token=EAAQdzGvlYm8BAPL8RowIoiFG8633iRwC1ZCEvorXe6aZCda8D8WAlJkm33lXKjeF1qZAhr5U3Xgvuqc7TOuZC2jfUUP1CNL8nqEt4xx6pAaW0UT8MZCgQjWA7ZCzAz9RFxfvGLcEWwZAhtoJYWGNxE4GpZBGLhxSbSXVMpNDla0HlI4NDWNEBwOwY7FzfdxBmZBZB95i00jty2olbPO0WPjAjKZCy7XO3TRGVoZD&fields=genre&limit=25&before=MzU3NDUxNDU4MzMy\\\"}}";
        Movies movies = Movies.serializeString(jsonData);
        assertNotEquals("Serializer returned null", null, movies.getData());
        assertEquals("Class doesn't return an empty array", 0, movies.getData().size());
    }

    @Test
    public void serializeStringTestNull() throws Exception {
        String jsonData = null;
        Movies movies = Movies.serializeString(jsonData);
        assertNotEquals("Serializer returned null", null, movies);
    }
}