package com.confusedpenguins.socialdeets;

/**
 * This seems like a bit of overkill but in the future logic could be added to the getters (ensure no nulls)
 * Additionally if the app changes to handle it's own serialization then we'll have something to test
 * Created by Andrew on 6/25/2017.
 */

import com.confusedpenguins.socialdeets.data.SocialProfile;

import org.junit.Test;

import static org.junit.Assert.*;

public class SocialProfileUnitTest {
    @Test
    public void serializeStringTest() throws Exception {
        String inputJson = "{\n" +
                "  'id': '10102687105196604',\n" +
                "  'name': 'Andrew Ally',\n" +
                "  'email': 'arrayindex@gmail.com',\n" +
                "  'picture': {\n" +
                "    'data': {\n" +
                "      'url': 'https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14495424_10102239352007584_5100950222189714797_n.jpg?oh=ab9a1efecaff0bf05390b9614c085241&oe=59D93588'\n" +
                "    }\n" +
                "  },\n" +
                "  'hometown': {\n" +
                "    'id': '111046035586548',\n" +
                "    'name': 'Jamaica, New York'\n" +
                "  }\n" +
                "}";
        SocialProfile actual = SocialProfile.serializeString(inputJson);
        System.out.println(actual.toString());
        assertNotEquals("Serializer Returned bad object", null, actual);

        assertEquals("Mocked ID should read Andrew Ally", "10102687105196604", actual.getUserId());
        assertEquals("Mocked Name should read Andrew Ally", "Andrew Ally", actual.getFullName());
        assertEquals("Mocked Email should read arrayindex@gmail.com", "arrayindex@gmail.com", actual.getEmail());
        assertEquals("Mocked Hometown should read Jamaica, New York", "Jamaica, New York", actual.getHometown().getName());
        assertEquals("Mocked Profile Picture should be a url",
                "https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/14495424_10102239352007584_5100950222189714797_n.jpg?oh=ab9a1efecaff0bf05390b9614c085241&oe=59D93588",
                actual.getPicture().getData().getUrl());

    }
}
