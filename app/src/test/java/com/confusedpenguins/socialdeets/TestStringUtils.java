package com.confusedpenguins.socialdeets;

import com.confusedpenguins.socialdeets.utils.StringUtils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Andrew on 6/25/2017.
 */
public class TestStringUtils {
    @Test
    public void testNormalStringArrayBuilder() throws Exception {
        String[] array = {"hello", "world", "foo", "bar"};
        String expected = "hello,world,foo,bar";
        String actual = StringUtils.arrayToBasicString(array);
        assertEquals("String Builder builds incorrectly for intended purpose", expected, actual);
    }

    @Test
    public void arrayToBasicStringTest() throws Exception {
        String[] array = null;
        String expected = "";
        String actual = StringUtils.arrayToBasicString(array);
        assertEquals("String Builder should return '' if null passed in", expected, actual);
    }

    @Test
    public void upperCaseFirstLetterTestNull() throws Exception {
        String input = null;
        String expected = "";
        String actual = StringUtils.upperCaseFirstLetter(input);
        assertEquals("Should return '' if null passed in", expected, actual);
    }

    @Test
    public void upperCaseFirstLetterTestNumber() throws Exception {
        String input = "7 up";
        String expected = "7 up";
        String actual = StringUtils.upperCaseFirstLetter(input);
        assertEquals("Numbers are already capitalized", expected, actual);
    }

    @Test
    public void upperCaseFirstLetterTest() throws Exception {
        String input = "hello world";
        String expected = "Hello world";
        String actual = StringUtils.upperCaseFirstLetter(input);
        assertEquals("First Letter should be capitalized", expected, actual);
    }
}
